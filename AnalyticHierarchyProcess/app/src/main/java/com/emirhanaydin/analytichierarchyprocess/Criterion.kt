package com.emirhanaydin.analytichierarchyprocess

data class Criterion(
    val criterion: String,
    var value: Int = 1
)