package com.emirhanaydin.analytichierarchyprocess

data class CriterionPriority(
    val criterion: String,
    val priority: Float
)